namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HOCONLINEs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Ten = c.String(),
                        Link = c.String(),
                        Hinh = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.HOCONLINEs");
        }
    }
}
