namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HOCONLINEUserProfiles",
                c => new
                    {
                        HOCONLINE_ID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.HOCONLINE_ID, t.UserProfile_UserId })
                .ForeignKey("dbo.HOCONLINEs", t => t.HOCONLINE_ID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId, cascadeDelete: true)
                .Index(t => t.HOCONLINE_ID)
                .Index(t => t.UserProfile_UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.HOCONLINEUserProfiles", new[] { "UserProfile_UserId" });
            DropIndex("dbo.HOCONLINEUserProfiles", new[] { "HOCONLINE_ID" });
            DropForeignKey("dbo.HOCONLINEUserProfiles", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.HOCONLINEUserProfiles", "HOCONLINE_ID", "dbo.HOCONLINEs");
            DropTable("dbo.HOCONLINEUserProfiles");
        }
    }
}
