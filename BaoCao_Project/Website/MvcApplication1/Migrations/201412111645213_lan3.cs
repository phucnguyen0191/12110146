namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BAIHOCs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenBaiHoc = c.String(),
                        Link = c.String(),
                        LOPID = c.Int(nullable: false),
                        MONHOCID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MONHOCs", t => t.MONHOCID, cascadeDelete: true)
                .ForeignKey("dbo.LOPs", t => t.LOPID, cascadeDelete: true)
                .Index(t => t.MONHOCID)
                .Index(t => t.LOPID);
            
            CreateTable(
                "dbo.LOPs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenLop = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MONHOCs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenMonHoc = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LOP_MONHOC",
                c => new
                    {
                        LOPID = c.Int(nullable: false),
                        MONHOCID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LOPID, t.MONHOCID })
                .ForeignKey("dbo.LOPs", t => t.LOPID, cascadeDelete: true)
                .ForeignKey("dbo.MONHOCs", t => t.MONHOCID, cascadeDelete: true)
                .Index(t => t.LOPID)
                .Index(t => t.MONHOCID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.LOP_MONHOC", new[] { "MONHOCID" });
            DropIndex("dbo.LOP_MONHOC", new[] { "LOPID" });
            DropIndex("dbo.BAIHOCs", new[] { "LOPID" });
            DropIndex("dbo.BAIHOCs", new[] { "MONHOCID" });
            DropForeignKey("dbo.LOP_MONHOC", "MONHOCID", "dbo.MONHOCs");
            DropForeignKey("dbo.LOP_MONHOC", "LOPID", "dbo.LOPs");
            DropForeignKey("dbo.BAIHOCs", "LOPID", "dbo.LOPs");
            DropForeignKey("dbo.BAIHOCs", "MONHOCID", "dbo.MONHOCs");
            DropTable("dbo.LOP_MONHOC");
            DropTable("dbo.MONHOCs");
            DropTable("dbo.LOPs");
            DropTable("dbo.BAIHOCs");
        }
    }
}
