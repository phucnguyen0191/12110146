﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class MONHOCController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /MONHOC/

        public ActionResult Index()
        {
            ViewBag.BaiHoc = db.BAIHOCs.ToList();
            return View(db.MONHOCs.ToList());
        }

        //
        // GET: /MONHOC/Details/5

        public ActionResult Details(int id = 0)
        {
            MONHOC monhoc = db.MONHOCs.Find(id);
            if (monhoc == null)
            {
                return HttpNotFound();
            }
            return View(monhoc);
        }

        //
        // GET: /MONHOC/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MONHOC/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MONHOC monhoc)
        {
            if (ModelState.IsValid)
            {
                db.MONHOCs.Add(monhoc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(monhoc);
        }

        //
        // GET: /MONHOC/Edit/5

        public ActionResult Edit(int id = 0)
        {
            MONHOC monhoc = db.MONHOCs.Find(id);
            if (monhoc == null)
            {
                return HttpNotFound();
            }
            return View(monhoc);
        }

        //
        // POST: /MONHOC/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MONHOC monhoc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(monhoc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(monhoc);
        }

        //
        // GET: /MONHOC/Delete/5

        public ActionResult Delete(int id = 0)
        {
            MONHOC monhoc = db.MONHOCs.Find(id);
            if (monhoc == null)
            {
                return HttpNotFound();
            }
            return View(monhoc);
        }

        //
        // POST: /MONHOC/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MONHOC monhoc = db.MONHOCs.Find(id);
            db.MONHOCs.Remove(monhoc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        public ActionResult LocTheoLop(int id)
        {

            //ViewBag.MonHoc = db.MONHOCs.ToList();
            //var t = db.LOPs.Where(p => p.ID == id).ToList();
            //ViewBag.baihoc = (from bh in db.BAIHOCs
            //              from l in db.LOPs
            //              from m in db.MONHOCs
            //              where bh.LOPID == l.ID && bh.MONHOCID == m.ID
            //              select new { bh.LOPID, bh.MONHOCID, bh.TenBaiHoc, bh.Link }).ToList();
            var r = db.BAIHOCs.Where(p => p.LOPID == id).OrderByDescending(p => p.MONHOCID).ToList();
            var MonHoc = db.MONHOCs.ToList();
            ViewBag.MonHocs = MonHoc;
            return View(r);
        }

    }
}