﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{

    public class BAIHOCController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /BAIHOC/

        public ActionResult Index()
        {
            var baihocs = db.BAIHOCs.Include(b => b.LOPs).Include(b => b.MONHOC);
            return View(baihocs.ToList());
        }

        //
        // GET: /BAIHOC/Details/5

        public ActionResult Details(int id = 0)
        {
            BAIHOC baihoc = db.BAIHOCs.Find(id);
            if (baihoc == null)
            {
                return HttpNotFound();
            }
            return View(baihoc);
        }

        //
        // GET: /BAIHOC/Create

        public ActionResult Create()
        {
            ViewBag.LOPID = new SelectList(db.LOPs, "ID", "TenLop");
            ViewBag.MONHOCID = new SelectList(db.MONHOCs, "ID", "TenMonHoc");
            return View();
        }

        //
        // POST: /BAIHOC/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BAIHOC baihoc)
        {
            if (ModelState.IsValid)
            {
                db.BAIHOCs.Add(baihoc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LOPID = new SelectList(db.LOPs, "ID", "TenLop", baihoc.LOPID);
            ViewBag.MONHOCID = new SelectList(db.MONHOCs, "ID", "TenMonHoc", baihoc.MONHOCID);
            return View(baihoc);
        }

        //
        // GET: /BAIHOC/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BAIHOC baihoc = db.BAIHOCs.Find(id);
            if (baihoc == null)
            {
                return HttpNotFound();
            }
            ViewBag.LOPID = new SelectList(db.LOPs, "ID", "TenLop", baihoc.LOPID);
            ViewBag.MONHOCID = new SelectList(db.MONHOCs, "ID", "TenMonHoc", baihoc.MONHOCID);
            return View(baihoc);
        }

        //
        // POST: /BAIHOC/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BAIHOC baihoc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(baihoc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LOPID = new SelectList(db.LOPs, "ID", "TenLop", baihoc.LOPID);
            ViewBag.MONHOCID = new SelectList(db.MONHOCs, "ID", "TenMonHoc", baihoc.MONHOCID);
            return View(baihoc);
        }

        //
        // GET: /BAIHOC/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BAIHOC baihoc = db.BAIHOCs.Find(id);
            if (baihoc == null)
            {
                return HttpNotFound();
            }
            return View(baihoc);
        }

        //
        // POST: /BAIHOC/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BAIHOC baihoc = db.BAIHOCs.Find(id);
            db.BAIHOCs.Remove(baihoc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}