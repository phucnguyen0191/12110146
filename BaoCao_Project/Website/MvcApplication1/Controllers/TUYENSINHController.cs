﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class TUYENSINHController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /TUYENSINH/

        public ActionResult Index()
        {
            return View(db.TUYENSINHs.ToList());
        }

        //
        // GET: /TUYENSINH/Details/5

        public ActionResult Details(int id = 0)
        {
            TUYENSINH tuyensinh = db.TUYENSINHs.Find(id);
            if (tuyensinh == null)
            {
                return HttpNotFound();
            }
            return View(tuyensinh);
        }

        //
        // GET: /TUYENSINH/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TUYENSINH/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TUYENSINH tuyensinh)
        {
            if (ModelState.IsValid)
            {
                db.TUYENSINHs.Add(tuyensinh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tuyensinh);
        }

        //
        // GET: /TUYENSINH/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TUYENSINH tuyensinh = db.TUYENSINHs.Find(id);
            if (tuyensinh == null)
            {
                return HttpNotFound();
            }
            return View(tuyensinh);
        }

        //
        // POST: /TUYENSINH/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TUYENSINH tuyensinh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tuyensinh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tuyensinh);
        }

        //
        // GET: /TUYENSINH/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TUYENSINH tuyensinh = db.TUYENSINHs.Find(id);
            if (tuyensinh == null)
            {
                return HttpNotFound();
            }
            return View(tuyensinh);
        }

        //
        // POST: /TUYENSINH/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TUYENSINH tuyensinh = db.TUYENSINHs.Find(id);
            db.TUYENSINHs.Remove(tuyensinh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}