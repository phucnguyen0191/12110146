﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    [Authorize]
    public class HOCONLINEController : Controller
    {
        private UsersContext db = new UsersContext();


        //
        // GET: /HOCONLINE/
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.TuyenSinh = db.TUYENSINHs.Take(3).ToList();
            ViewBag.Video = db.VIDEOs.Take(3).ToList();
            ViewBag.BaiHoc = db.BAIHOCs.ToList();
            ViewBag.MonHoc = db.MONHOCs.Take(2).ToList();
            return View(db.HOCONLINEs.ToList());
        }

        //
        // GET: /HOCONLINE/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            HOCONLINE hoconline = db.HOCONLINEs.Find(id);
            if (hoconline == null)
            {
                return HttpNotFound();
            }
            return View(hoconline);
        }

        //
        // GET: /HOCONLINE/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /HOCONLINE/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HOCONLINE hoconline)
        {
            if (ModelState.IsValid)
            {
                db.HOCONLINEs.Add(hoconline);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(hoconline);
        }

        //
        // GET: /HOCONLINE/Edit/5
        
        public ActionResult Edit(int id = 0)
        {
            HOCONLINE hoconline = db.HOCONLINEs.Find(id);
            if (hoconline == null)
            {
                return HttpNotFound();
            }
            return View(hoconline);
        }

        //
        // POST: /HOCONLINE/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HOCONLINE hoconline)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoconline).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hoconline);
        }

        //
        // GET: /HOCONLINE/Delete/5

        public ActionResult Delete(int id = 0)
        {
            HOCONLINE hoconline = db.HOCONLINEs.Find(id);
            if (hoconline == null)
            {
                return HttpNotFound();
            }
            return View(hoconline);
        }

        //
        // POST: /HOCONLINE/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HOCONLINE hoconline = db.HOCONLINEs.Find(id);
            db.HOCONLINEs.Remove(hoconline);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}