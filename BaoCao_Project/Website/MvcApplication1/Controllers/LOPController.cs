﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class LOPController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /LOP/

        public ActionResult Index()
        {
            return View(db.LOPs.ToList());
        }

        //
        // GET: /LOP/Details/5

        public ActionResult Details(int id = 0)
        {
            LOP lop = db.LOPs.Find(id);
            if (lop == null)
            {
                return HttpNotFound();
            }
            return View(lop);
        }

        //
        // GET: /LOP/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LOP/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LOP lop)
        {
            if (ModelState.IsValid)
            {
                db.LOPs.Add(lop);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lop);
        }

        //
        // GET: /LOP/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LOP lop = db.LOPs.Find(id);
            if (lop == null)
            {
                return HttpNotFound();
            }
            return View(lop);
        }

        //
        // POST: /LOP/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LOP lop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lop).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lop);
        }

        //
        // GET: /LOP/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LOP lop = db.LOPs.Find(id);
            if (lop == null)
            {
                return HttpNotFound();
            }
            return View(lop);
        }

        //
        // POST: /LOP/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LOP lop = db.LOPs.Find(id);
            db.LOPs.Remove(lop);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}