﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class VIDEOController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /VIDEO/

        public ActionResult Index()
        {
            return View(db.VIDEOs.ToList());
        }

        //
        // GET: /VIDEO/Details/5

        public ActionResult Details(int id = 0)
        {
            VIDEO video = db.VIDEOs.Find(id);
            if (video == null)
            {
                return HttpNotFound();
            }
            return View(video);
        }

        //
        // GET: /VIDEO/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /VIDEO/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VIDEO video)
        {
            if (ModelState.IsValid)
            {
                db.VIDEOs.Add(video);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(video);
        }

        //
        // GET: /VIDEO/Edit/5

        public ActionResult Edit(int id = 0)
        {
            VIDEO video = db.VIDEOs.Find(id);
            if (video == null)
            {
                return HttpNotFound();
            }
            return View(video);
        }

        //
        // POST: /VIDEO/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VIDEO video)
        {
            if (ModelState.IsValid)
            {
                db.Entry(video).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(video);
        }

        //
        // GET: /VIDEO/Delete/5

        public ActionResult Delete(int id = 0)
        {
            VIDEO video = db.VIDEOs.Find(id);
            if (video == null)
            {
                return HttpNotFound();
            }
            return View(video);
        }

        //
        // POST: /VIDEO/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VIDEO video = db.VIDEOs.Find(id);
            db.VIDEOs.Remove(video);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}