﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class HOCONLINE
    {
        public int ID { get; set; }
        [Display(Name="Tên Web")]
        public string Ten {get;set;}
        [Display(Name="Link")]
        public string Link { get; set; }
        [Display(Name="Hình minh họa")]
        public string Hinh { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}