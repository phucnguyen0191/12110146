﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class LOP
    {
        public int ID { get; set; }
        public string TenLop { get; set; }
        public virtual ICollection<MONHOC> MONHOCs { get; set; }
    }
}