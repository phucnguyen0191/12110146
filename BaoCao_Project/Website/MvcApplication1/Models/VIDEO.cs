﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class VIDEO
    {
        public int ID { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
    }
}