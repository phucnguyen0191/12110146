﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class BAIHOC
    {
        public int ID { get; set; }
        public string TenBaiHoc { get; set; }
        public string Link { get; set; }
        public virtual LOP LOPs { get; set; }
        public int LOPID { get; set; }
        public virtual MONHOC MONHOC { get; set; }
        public int MONHOCID { get; set; }
    }
}