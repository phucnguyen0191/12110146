﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class MONHOC
    {
        public int ID { get; set; }
        public string TenMonHoc { get; set; }
        public virtual ICollection<LOP> LOPs { get; set; }
        public virtual ICollection<BAIHOC> BAIHOCs { get; set; }
    }
}