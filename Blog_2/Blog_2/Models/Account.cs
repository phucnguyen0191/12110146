﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        
        public int AccountID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu!!!")]
        [DataType(DataType.Password)]

        public string Password { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập Email của bạn!!!")]
        [RegularExpression("^\\D\\w*@(\\w+\\.\\w+)+", ErrorMessage = "Nhập sai rồi! Nhập lại!")]
        public string Email { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập!")]
        [MaxLength(100, ErrorMessage = "Tối đa 100 ký tự!!!")]
        public string FirstName { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập!")]
        [MaxLength(100, ErrorMessage = "Tối đa 100 ký tự!!!")]
        public string LastName { set; get; }
        //
        public virtual ICollection<Post> Posts { set; get; }
    }
}