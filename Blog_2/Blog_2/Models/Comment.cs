﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung!!!")]
        [MinLength(50, ErrorMessage = "Tối thiểu phải có 50 ký tự!!!")]
        public string Body { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Phải nhập dữ liệu!!!")]     
        public DateTime DayCreated { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Phải nhập dữ liệu!!!")]
        public DateTime DayUpdated { set; get; }
        [Required(ErrorMessage = "Phải nhập bạn ơi!!!")]
        public string Author { set; get; }
        //
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
        

    }
}