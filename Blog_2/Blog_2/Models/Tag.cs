﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung!!!")]
        [MinLength(10, ErrorMessage = "Nhập ít nhất 10 ký tự!!!")]
        [MaxLength(100, ErrorMessage = "Tối đa chỉ được nhập 100 ký tự!!!")]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
        
    }
}