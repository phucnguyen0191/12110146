﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage="Vui lòng nhập tiêu đề!!!")]
        [MinLength(20, ErrorMessage = "Tối thiểu phải có 20 ký tự!!!")]
        [MaxLength(500,ErrorMessage="Tối đa chỉ được 500 ký tự!!!")]
        public string Title { set; get; }
        [Required(ErrorMessage="Vui lòng nhập nội dung!!!")]
        [MinLength(50,ErrorMessage="Tối thiểu phải có 50 ký tự!!!")]
        public string Body { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Phải nhập dữ liệu!!!")]
        public DateTime DayCreated { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Phải nhập dữ liệu!!!")]
        public DateTime DayUpdated { set; get; }
        //
        [Required(ErrorMessage="Phải chọn accounnt")]
        public int AccountID { set; get; }
        public virtual Account Account { set; get; }
        //
        public virtual ICollection<Comment> Comments { set; get; }
        //
        public virtual ICollection<Tag> Tags { set; get; }
        //
        
    }
}