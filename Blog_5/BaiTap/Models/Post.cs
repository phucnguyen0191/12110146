﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaiTap.Models
{
    public class Post
    {
        public int ID { get; set; }
        [Required(ErrorMessage="Vui lòng nhâ[j tiêu đề!")]
        [MinLength(20,ErrorMessage="Tối thiểu phải có 20 ký tự!")]
        [MaxLength(500,ErrorMessage="Tối đa chỉ được 500 ký tự!")]
        public String Title { get; set; }
        [Required (ErrorMessage="Vui lòng nhập nội dung!")]
        [MinLength(50,ErrorMessage="Nhập tối thiểu 50 ký tự!")]
        public String Body { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DayUpdated { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public int UserProfileUserId { get; set; }
    }
}