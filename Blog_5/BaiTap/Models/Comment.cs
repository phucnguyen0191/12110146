﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaiTap.Models
{
    public class Comment
    {
        
        public int ID { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung!")]
        [MinLength(50, ErrorMessage = "Nhập tối thiểu 50 ký tự!")]
        public String Body { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DayUpdated { get; set; }
        [Required(ErrorMessage="Vui lòng nhậpn tên người viết!")]
        public String Author { get; set; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DayCreated).Minutes;
            }
        }
        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}