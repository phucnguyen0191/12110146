﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaiTap.Models
{
    public class Tag
    {
        public int ID { get; set; }
        [Required(ErrorMessage="Vui lòng nhập!")]
        [MinLength(10,ErrorMessage="Tối thiểu 10 ký tự!")]
        [MaxLength(100,ErrorMessage="Tối đa chỉ được 100 ký tự!")]
        public String Content { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}